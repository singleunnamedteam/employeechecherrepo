package NetModule;

import android.os.StrictMode;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import NetModule.Models.Detector;
import NetModule.Models.Employee;
import pi_14_3.unnamed.paul.employeechecker_mobile.R;

/**
 * Created by Paul on 16.11.2016.
 */
public class NetModule {
    static {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public static String getData(String url) {
        DefaultHttpClient   httpclient = new DefaultHttpClient(new BasicHttpParams());
        HttpGet httpGet = new HttpGet(url);
// Depends on your web service
        httpGet.setHeader("Content-type", "application/json");

        InputStream inputStream = null;
        try {
            HttpResponse response = httpclient.execute(httpGet);
            HttpEntity entity = response.getEntity();

            inputStream = entity.getContent();
            // json is UTF-8 by default
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 20);
            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null)
            {
                sb.append(line);
            }

            return sb.toString();
        } catch (Exception e) {
            // Oops
            e.printStackTrace();
            return null;
        }
        finally {
            try{if(inputStream != null)inputStream.close();}catch(Exception squish){}
        }
    }


    public static Employee[] getEmployees(String url){
        String data = getData(url);
        Gson gson = new Gson();
        try {
            Employee[] result = gson.fromJson(data, Employee[].class);
            return result;
        }
        catch (Exception ex){
            return null;
        }
    }

    public static Detector[] getDetectors(String url){
        String data = getData(url);

        Gson gson = new Gson();

        try {
            Detector[] result = gson.fromJson(data, Detector[].class);
            return result;
        }
        catch (Exception e){
            return null;
        }
    }
}
