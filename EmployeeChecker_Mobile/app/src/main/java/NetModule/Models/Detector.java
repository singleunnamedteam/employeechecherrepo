package NetModule.Models;

/**
 * Created by Paul on 16.11.2016.
 */
public class Detector {
    public int Id;
    public String Placement;

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getPlacement() {
        return Placement;
    }

    public void setPlacement(String Placement) {
        this.Placement = Placement;
    }
}
