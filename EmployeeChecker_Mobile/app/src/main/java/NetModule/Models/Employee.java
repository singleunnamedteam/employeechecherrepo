package NetModule.Models;

/**
 * Created by Paul on 16.11.2016.
 */
public class Employee {
    public int Id;
    public String FIO;
    public String Post;

    public int getId() {
        return Id;
    }

    public String getFio() {
        return FIO;
    }

    public String getPost() {
        return Post;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public void setFio(String fio) {
        this.FIO = fio;
    }

    public void setPost(String post) {
        this.Post = post;
    }
}
