package pi_14_3.unnamed.paul.employeechecker_mobile;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import NetModule.Models.Employee;
import NetModule.NetModule;

public class EmployeesActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Employee[] employees = NetModule.getEmployees(getResources().getString(R.string.employees_url));

        setContentView(R.layout.activity_employees);
        super.onCreate(savedInstanceState);

        if (employees != null && employees.length > 0){
            // создаем адаптер
            EmployeeAdapter adapter = new EmployeeAdapter(employees);
            setListAdapter(adapter);
            return;
        }

        if (employees == null){
            String[] emptyArr = { "Server does not respond" };
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, emptyArr);

            setListAdapter(adapter);
            return;
        }

        if (employees.length == 0){
            String[] emptyArr = { "You have no employee. Register new one with web application." };
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, emptyArr);

            setListAdapter(adapter);
            return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.employee_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id){
            case R.id.Main:
                Intent intent1 = new Intent(this, MainActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);
                return true;
            case R.id.detectors:
                Intent intent2 = new Intent(this, DetectorsActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private Employee getModel(int position) {
        return(((EmployeeAdapter)getListAdapter()).getItem(position));
    }

    public class EmployeeAdapter extends ArrayAdapter<Employee> {

        private LayoutInflater mInflater;

        public EmployeeAdapter(Employee[] list) {
            super(EmployeesActivity.this, R.layout.employees_list_view,  list);
            mInflater = LayoutInflater.from(EmployeesActivity.this);
        }

        public View getView(int position, View convertView,
                            ViewGroup parent) {
            ViewHolder holder;
            View row=convertView;
            if(row==null){

                row = mInflater.inflate(R.layout.employees_list_view, parent, false);
                holder = new ViewHolder();
                holder.FIO = (TextView) row.findViewById(R.id.FIO);
                holder.Post = (TextView) row.findViewById(R.id.Post);
                row.setTag(holder);
            }
            else{

                holder = (ViewHolder)row.getTag();
            }

            Employee employee = getModel(position);

            holder.FIO.setText(employee.getFio());
            holder.Post.setText(employee.getPost());

            return row;
        }

        class ViewHolder {
            public TextView FIO, Post;
        }
    }
}