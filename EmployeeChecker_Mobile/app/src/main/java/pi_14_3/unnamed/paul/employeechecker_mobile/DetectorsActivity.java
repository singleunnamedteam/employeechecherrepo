package pi_14_3.unnamed.paul.employeechecker_mobile;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

    import NetModule.Models.Detector;
    import NetModule.NetModule;

public class DetectorsActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Detector[] detectors = NetModule.getDetectors(getResources().getString(R.string.detectors_url));
        super.onCreate(savedInstanceState);

        if (detectors != null && detectors.length > 0){
            // создаем адаптер
            DetectorAdapter adapter = new DetectorAdapter(detectors);
            setListAdapter(adapter);
        }

        if (detectors == null){
            String[] emptyArr = { "Server does not respond" };
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, emptyArr);

            setListAdapter(adapter);
            return;
        }

        if (detectors.length == 0){
            String[] emptyArr = { "You have no detector. You can register one with web application." };
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, emptyArr);

            setListAdapter(adapter);
            return;
        }

        setContentView(R.layout.activity_detectors);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.employee_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id){
            case R.id.Main:
                Intent intent1 = new Intent(this, MainActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);
                return true;
            case R.id.detectors:
                Intent intent2 = new Intent(this, DetectorsActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private Detector getModel(int position) {
        return(((DetectorAdapter)getListAdapter()).getItem(position));
    }

    public class DetectorAdapter extends ArrayAdapter<Detector> {

        private LayoutInflater mInflater;

        public DetectorAdapter(Detector[] list) {
            super(DetectorsActivity.this, R.layout.detectors_list_view,  list);
            mInflater = LayoutInflater.from(DetectorsActivity.this);
        }

        public View getView(int position, View convertView,
                            ViewGroup parent) {
            ViewHolder holder;
            View row=convertView;
            if(row==null){

                row = mInflater.inflate(R.layout.detectors_list_view, parent, false);
                holder = new ViewHolder();
                holder.Id = (TextView) row.findViewById(R.id.Id);
                holder.Placement = (TextView) row.findViewById(R.id.Placement);
                row.setTag(holder);
            }
            else{

                holder = (ViewHolder)row.getTag();
            }

            Detector detector = getModel(position);

            holder.Id.setText(detector.getId());
            holder.Placement.setText(detector.getPlacement());

            return row;
        }

        class ViewHolder {
            public TextView Id, Placement;
        }
    }
}
