﻿using EmployeeChecker_DAL.EF;
using EmployeeChecker_DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleDBInitializer
{
    class Program
    {
        static void Main(string[] args)
        {
            DBContext db = new DBContext();
            db.Employees.Add(new Employee()
            {
                FIO = "Test",
                Post = "Test"
            });

            db.SaveChanges();
        }
    }
}
