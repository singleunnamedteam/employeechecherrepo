﻿using EmployeeChecker_WEB.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace EmployeeChecker_WEB.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Employees()
        {
            WebRequest request = WebRequest.Create("http://localhost:44091/api/Employees/GetEmployees");
            WebResponse response = request.GetResponse();

            StringBuilder content = new StringBuilder(200);
            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    String line = "";
                    while ((line = reader.ReadLine()) != null)
                    {
                        content.Append(line);
                    }
                }
            }

            List<Employee> m = JsonConvert.DeserializeObject<List<Employee>>(content.ToString());

            return View(m);
        }

        public ActionResult Detectors()
        {
            WebRequest request = WebRequest.Create("http://localhost:44091/api/Detectors/GetDetectors");
            WebResponse response = request.GetResponse();

            StringBuilder content = new StringBuilder(200);
            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    String line = "";
                    while ((line = reader.ReadLine()) != null)
                    {
                        content.Append(line);
                    }
                }
            }

            List<Detector> m = JsonConvert.DeserializeObject<List<Detector>>(content.ToString());

            return View(m);
        }
    }
}
