﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeChecker_WEB.Models
{
    public class Detector
    {
        public Int32 Id { get; set; }
        public String Placement { get; set; }
    }
}
