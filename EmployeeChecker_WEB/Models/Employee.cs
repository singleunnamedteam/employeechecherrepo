﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeChecker_WEB.Models
{
    public class Employee
    {
        public Int32 Id { get; set; }
        public String FIO { get; set; }
        public String Post { get; set; }
    }
}