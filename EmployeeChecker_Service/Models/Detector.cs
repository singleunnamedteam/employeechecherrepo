﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeCheckerService.Models
{
    public class Detector
    {
        public Int32 Id { get; set; }
        public String Placement { get; set; }
    }
}
