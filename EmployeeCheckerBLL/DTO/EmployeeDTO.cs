﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeCheckerBLL.DTO
{
    public class EmployeeDTO
    {
        public Int32 Id { get; set; }
        public String FIO { get; set; }
        public String Post { get; set; }
    }
}
