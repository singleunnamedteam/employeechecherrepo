﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeCheckerBLL.DTO
{
    public class CheckDTO
    {
        public Int32 Id { get; set; }
        public Int32 EmployeeId { get; set; }
        public Int32 DetectorId { get; set; }
        public DateTime Date { get; set; }
    }
}
