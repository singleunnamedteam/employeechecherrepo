﻿using EmployeeCheckerBLL.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeCheckerBLL.Services
{
    public class DetectorService
    {
        public String GetDetectors()
        {
            WebRequest request = WebRequest.Create("http://localhost:3383/api/Detectors/GetDetectors");
            WebResponse response = request.GetResponse();

            StringBuilder content = new StringBuilder(200);
            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    String line = "";
                    while ((line = reader.ReadLine()) != null)
                    {
                        content.Append(line);
                    }
                }
            }

            return content.ToString();
        }
    }
}
