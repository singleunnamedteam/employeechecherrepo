﻿using EmployeeChecker_DAL.EF;
using EmployeeChecker_DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EmployeeCheckerDAL_API.Controllers
{
    public class DetectorsController : ApiController
    {
        DBContext context = new DBContext();
        public List<Detector> GetDetectors()
        {
            List<Detector> detectors = context.Detectors.ToList();
            return detectors;
        }
    }
}
