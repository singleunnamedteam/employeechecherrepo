﻿using EmployeeChecker_DAL.EF;
using EmployeeChecker_DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EmployeeCheckerDAL_API.Controllers
{
    public class EmployeesController : ApiController
    {
        DBContext context = new DBContext();
        public List<Employee> GetEmployees()
        {
            List<Employee> detectors = context.Employees.ToList();
            return detectors;
        }
    }
}
