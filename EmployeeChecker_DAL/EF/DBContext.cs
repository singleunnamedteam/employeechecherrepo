﻿using EmployeeChecker_DAL.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace EmployeeChecker_DAL.EF
{
    public class DBContext: DbContext
    {
        public DBContext()
            : base(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Files\Projects\EmployeeChecker\EmployeeChecker_DAL\DB\Database1.mdf;Integrated Security=True")
        { }

        public DBContext(String connectionString): base(connectionString)
        {

        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Detector> Detectors { get; set; }
        public DbSet<Check> Checks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // использование Fluent API

            modelBuilder.Entity<Employee>().HasKey(e => e.Id);
            modelBuilder.Entity<Detector>().HasKey(d => d.Id);
            modelBuilder.Entity<Check>().HasKey(c => c.Id);

            modelBuilder.Entity<Employee>().HasMany(e => e.Checks).WithRequired(c => c.Employee).WillCascadeOnDelete(false);
            modelBuilder.Entity<Detector>().HasMany(e => e.Checks).WithRequired(c => c.Detector).WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }
    }
}
