﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmployeeChecker_DAL.Models
{
    public class Detector
    {
        public Int32 Id { get; set; }
        public String Placement { get; set; }

        public ICollection<Check> Checks { get; set; }
    }
}
