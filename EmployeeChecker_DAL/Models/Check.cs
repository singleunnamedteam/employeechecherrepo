﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmployeeChecker_DAL.Models
{
    public class Check
    {
        public Int32 Id { get; set; }
        public Int32 EmployeeId { get; set; }
        public Int32 DetectorId { get; set; }
        public DateTime Date { get; set; }

        public Employee Employee { get; set; }
        public Detector Detector { get; set; }
    }
}
