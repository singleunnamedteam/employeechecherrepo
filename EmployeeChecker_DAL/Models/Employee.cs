﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmployeeChecker_DAL.Models
{
    public class Employee
    {
        public Int32 Id { get; set; }
        public String FIO { get; set; }
        public String Post { get; set; }

        public ICollection<Check> Checks { get; set; }
    }
}
