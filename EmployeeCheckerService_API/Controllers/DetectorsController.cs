﻿using EmployeeCheckerService_API.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Mvc;

namespace EmployeeCheckerService_API.Controllers
{
    public class DetectorsController : ApiController
    {
        public List<Detector> GetDetectors()
        {
            WebRequest request = WebRequest.Create("http://localhost:12331/api/Detectors/GetDetectors");
            WebResponse response = request.GetResponse();

            StringBuilder content = new StringBuilder(200);
            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    String line = "";
                    while ((line = reader.ReadLine()) != null)
                    {
                        content.Append(line);
                    }
                }
            }

            List<Detector> m = JsonConvert.DeserializeObject<List<Detector>>(content.ToString());

            return m;
        }
    }
}
