﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;

using EmployeeCheckerService_API.Models;
using Newtonsoft.Json;

namespace EmployeeCheckerService_API.Controllers
{
    public class EmployeesController : ApiController
    {
        public List<Employee> GetEmployees()
        {
            WebRequest request = WebRequest.Create("http://localhost:12331/api/Employees/GetEmployees");
            WebResponse response = request.GetResponse();

            StringBuilder content = new StringBuilder(200);
            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    String line = "";
                    while ((line = reader.ReadLine()) != null)
                    {
                        content.Append(line);
                    }
                }
            }

            List<Employee> m = JsonConvert.DeserializeObject<List<Employee>>(content.ToString());

            return m;
        }
    }
}
