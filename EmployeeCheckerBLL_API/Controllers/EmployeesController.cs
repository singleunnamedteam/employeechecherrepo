﻿using EmployeeCheckerBLL.DTO;
using EmployeeCheckerBLL.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Http;
using System.Web.Mvc;

namespace EmployeeCheckerBLL_API.Controllers
{
    public class EmployeesController : ApiController
    {
        EmployeeService service = new EmployeeService();

        public List<EmployeeDTO> GetEmployees()
        {
            List<EmployeeDTO> m = JsonConvert.DeserializeObject<List<EmployeeDTO>>(service.GetEmployees());
            return m;
        }
    }
}
