﻿using EmployeeCheckerBLL.DTO;
using EmployeeCheckerBLL.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Mvc;

namespace EmployeeCheckerBLL_API.Controllers
{
    public class DetectorsController : ApiController
    {
        DetectorService service = new DetectorService();

        public List<DetectorDTO> GetDetectors()
        {
            List<DetectorDTO> m = JsonConvert.DeserializeObject<List<DetectorDTO>>(service.GetDetectors());
            return m;
        }
    }
}
